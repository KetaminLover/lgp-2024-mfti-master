package main

import (
    "fmt"
    "gitlab.com/arseniy.shtunder/task-1/pkg/input"
    solver "gitlab.com/arseniy.shtunder/task-1/internal/solve"
)

func main() {
    for {
        num1 := input.InputData("Enter the first number: ")
        operator := input.InputOperator("Enter the operator (-, +, *, /): ")
        num2 := input.InputData("Enter the second number: ")
        res, err := solver.Solve(num1, num2, operator)
        if err != nil {
            fmt.Println(err)
        } else {
            fmt.Printf("result of expression: %v %s %v = %v\n\n", num1, operator, num2, res)
        }
    }
}