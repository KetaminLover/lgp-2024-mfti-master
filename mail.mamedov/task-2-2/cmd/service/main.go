package main

import (
	"container/heap"
	"fmt"
)

// IntHeap is a max-heap of int.
type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] > h[j] } // Max heap
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func findKthGreatest(arr []int, k int) int {
	h := &IntHeap{}

	// Initialize the max-heap
	for _, num := range arr {
		heap.Push(h, num)
	}

	// Pop k-1 elements from the heap to find the kth greatest value
	for i := 0; i < k-1; i++ {
		heap.Pop(h)
	}

	return (*h)[0]
}

func main() {
	var N, K int

	_, err := fmt.Scanln(&N)
	if err != nil {
		panic(err)
	}

	dishesList := make([]int, N)
	for i := 0; i < N-1; i++ {
		_, err = fmt.Scan(&dishesList[i])
		if err != nil {
			panic(err)
		}
	}

	_, err = fmt.Scanln(&dishesList[N-1])
	if err != nil {
		panic(err)
	}

	_, err = fmt.Scanln(&K)
	if err != nil {
		panic(err)
	}

	if K <= N {
		fmt.Println(findKthGreatest(dishesList, K))
	} else {
		fmt.Println(-1)
	}
}
